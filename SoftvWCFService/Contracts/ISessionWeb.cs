﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using Softv.Entities;
namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISessionWeb   {
      
    
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddVendedor(string nombre, string domicilio, bool? activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddDepartamento(string nombre, float? metro, bool? activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddAmenitie", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddAmenitie(string nombre, string desc, bool? activo);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddVista", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddVista(string nombre, string desc, bool? activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Addusuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? Addusuario(string nombre, string correo, string contrasenia, bool? activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVendedorN", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoMobiliti> GetVendedorN(string nombre);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVista", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoMobiliti> GetVista();



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoMobiliti> GetVendedor();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUsuarios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoUser> GetUsuarios();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUsuariosnN", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoUser> GetUsuariosnN(string nombre);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVistaN", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoMobiliti> GetVistaN(string nombre);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSpaceDept", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoDepto> GetSpaceDept();
       

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSpaceDeptN", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoDepto> GetSpaceDeptN(string nombre);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAmenitie", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoMobiliti> GetAmenitie();



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAmenitieN", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InfoMobiliti> GetAmenitieN(string nombre);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        InfoMobiliti UpdateVendedor(string nombre, int? clv_vendedor, string domicilio, bool? activo);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateVista", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        InfoMobiliti UpdateVista(string nombre, int? clv_vista, string descripcion, bool? activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateAmenitie", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        InfoMobiliti UpdateAmenitie(string nombre, int? clv_amenitie, string descripcion, bool? activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateUsuarios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        InfoMobiliti UpdateUsuarios(string nombre, int? clv_usuario, string correo, string contrasenia, bool? activo);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        InfoMobiliti UpdateDepartamento(string nombre, int? clv_espacio_departamento, float metros_cuadrados, string contrasenia, bool? activo);















    }
}

