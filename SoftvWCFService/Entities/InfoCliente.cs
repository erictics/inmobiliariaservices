﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;


namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class InfoCliente
    {
        [DataMember]
        public long? Contrato { get; set; }


        [DataMember]
        public string ContratoCompuesto { get; set; }


        [DataMember]
        public string Cliente { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string password { get; set; }

        [DataMember]
        public string Serie { get; set; }

        [DataMember]
        public string token { get; set; }

        [DataMember]
        public bool  ? valid { get; set; }

        [DataMember]
        public string message { get; set; }

        [DataMember]
        public string nombre { get; set; }


        [DataMember]
        public int? clv_vista { get; set; }

        [DataMember]
        public string descripcion { get; set; }

        [DataMember]
        public
        bool? activo { get; set; }

        [DataMember]
        public bool? Resultado { get; set; }







    }

    public class InfoMobiliti
    {
       
        [DataMember]
        public string nombre { get; set; }


        [DataMember]
        public int? clv_vista { get; set; }

        [DataMember]
        public int? clv_vendedor { get; set; }

        [DataMember]
        public int? clv_amenitie { get; set; }


        [DataMember]
        public string descripcion { get; set; }

        [DataMember]
        public
        bool? activo { get; set; }

        [DataMember]
        public string domicilio { get; set; }

    }

    public class InfoUser
    {

        [DataMember]
        public string nombre { get; set; }


        [DataMember]
        public int? clv_usuario { get; set; }

    
    

        [DataMember]
        public string correo { get; set; }

        [DataMember]
        public
        bool? activo
        { get; set; }

        [DataMember]
        public string contrasenia { get; set; }

    }

    public class InfoDepto
    {

        [DataMember]
        public string nombre { get; set; }


        [DataMember]
        public int? clv_espacio_departamento { get; set; }

        [DataMember]
        public float? metros_cuadrados { get; set; }



        [DataMember]
        public
        bool? activo
        { get; set; }

      

    }











}
