﻿
using Microsoft.IdentityModel.Tokens;
using SoftvWCFService.Contracts;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using Softv.Entities;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;

namespace SoftvWCFService
{
    [ScriptService]
    public partial class SoftvWCFService : ISessionWeb
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        #region inserts


        public int? AddVendedor(string nombre, string domicilio, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("insert_vendedor @nombre ,@domicilio,@activo", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@domicilio", domicilio);
                        command.Parameters.AddWithValue("@activo", activo);

                        InfoCliente i = new InfoCliente();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());





                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error insert_vendedor " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public int? AddDepartamento(string nombre, float? metro, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("insert_espacio_departamento @nombre ,@metros_cuadrados,@activo", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@metros_cuadrados", metro);
                        command.Parameters.AddWithValue("@activo", activo);

                        InfoCliente i = new InfoCliente();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());





                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error AddDepartamento " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public int? AddAmenitie(string nombre, string desc, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("insert_amenitie @nombre ,@descripcion,@activo", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@descripcion", desc);
                        command.Parameters.AddWithValue("@activo", activo);

                        InfoCliente i = new InfoCliente();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());





                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error AddAmenitie " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }




        public int? AddVista(string nombre, string desc, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("insert_vista @nombre, @descripcion,@activo", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@descripcion", desc);
                        command.Parameters.AddWithValue("@activo", activo);

                        InfoCliente i = new InfoCliente();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());





                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro insert_vista " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public int? Addusuario(string nombre, string correo, string contrasenia, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("insert_usuarios @nombre, @correo,@contrasenia,@activo", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@correo", correo);
                        command.Parameters.AddWithValue("@contrasenia", contrasenia);
                        command.Parameters.AddWithValue("@activo", activo);

                        InfoCliente i = new InfoCliente();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());





                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro insert_vista " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }













        #endregion


        #region gets
        public List<InfoMobiliti> GetVendedorN(string nombre)
        {
            List<InfoMobiliti> Vendedoreslist = new List<InfoMobiliti>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_vendedor_nombre  @nombre", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);

                 
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoMobiliti i = new InfoMobiliti();
                                i.clv_vista = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                i.descripcion = rd[2].ToString();
                                i.activo = bool.Parse(rd[3].ToString());
                                Vendedoreslist.Add(i);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro select_vendedor_nombre " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vendedoreslist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }



        public List<InfoMobiliti> GetVista()
        {
            List<InfoMobiliti> Vistalist = new List<InfoMobiliti>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_vista  ", connection);


                     
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoMobiliti i = new InfoMobiliti();
                                i.clv_vista = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                i.descripcion = rd[2].ToString();
                                i.activo = bool.Parse(rd[3].ToString());
                                Vistalist.Add(i);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_vista " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vistalist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public List<InfoMobiliti> GetVendedor()

              
        {
            List<InfoMobiliti> Vendedorlist = new List<InfoMobiliti>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_vendedor  ", connection);


                
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoMobiliti j = new InfoMobiliti();
                                j.clv_vendedor = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                                j.domicilio = rd[2].ToString();
                                j.activo = bool.Parse(rd[3].ToString());
                                Vendedorlist.Add(j);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_vista " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vendedorlist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }



        public List<InfoUser> GetUsuarios()
        {

            List<InfoUser> Devolucionusuarioslist = new List<InfoUser>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_usuario", connection);


                      
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoUser j = new InfoUser();
                                j.clv_usuario = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                                j.correo = rd[2].ToString();
                                j.contrasenia = rd[3].ToString();
                                j.activo = bool.Parse(rd[4].ToString());
                                Devolucionusuarioslist.Add(j);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_vista " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Devolucionusuarioslist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public List<InfoUser> GetUsuariosnN(string nombre)
        {
            List<InfoUser> usuarioslist = new List<InfoUser>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string result = "";
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_usuario_nombre  @nombre", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);

                   
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoUser i = new InfoUser();
                                i.clv_usuario = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                i.correo = rd[2].ToString();
                                i.activo = bool.Parse(rd[3].ToString());
                                usuarioslist.Add(i);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro select_usuario_nombre " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return usuarioslist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }



        public List<InfoMobiliti> GetVistaN(string nombre)
        {

            List<InfoMobiliti>  Vlist = new List<InfoMobiliti>();


            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_vista_nombre  @nombre", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);

                       
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoMobiliti i = new InfoMobiliti();
                                i.clv_vista = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                i.descripcion = rd[2].ToString();
                                i.activo = bool.Parse(rd[3].ToString());
                                Vlist.Add(i);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro select_vista_nombre " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vlist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public List<InfoDepto> GetSpaceDept()
        {
            List<InfoDepto> Infolist = new List<InfoDepto>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_espacio_departamento", connection);


                      
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoDepto j = new InfoDepto();
                                j.clv_espacio_departamento = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                                j.metros_cuadrados = float.Parse(rd[0].ToString());
                                j.activo = bool.Parse(rd[3].ToString());
                                Infolist.Add(j);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_vista " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Infolist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public List<InfoDepto> GetSpaceDeptN(string nombre)
        {
            List<InfoDepto> Infolist = new List<InfoDepto>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_espacio_departamento_nombre @nombre", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);


                       
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoDepto j = new InfoDepto();
                                j.clv_espacio_departamento = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                                j.metros_cuadrados = float.Parse(rd[0].ToString());
                                j.activo = bool.Parse(rd[3].ToString());
                                Infolist.Add(j);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error select_vista " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Infolist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public List<InfoMobiliti> GetAmenitie()
        {
            List<InfoMobiliti> Vlist = new List<InfoMobiliti>();

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_amenitie", connection);


                      
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoMobiliti j = new InfoMobiliti();
                                j.clv_amenitie = Int32.Parse(rd[0].ToString());
                                j.nombre = rd[1].ToString();
                                j.descripcion = rd[0].ToString();
                                j.activo = bool.Parse(rd[3].ToString());
                                Vlist.Add(j);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Amenities " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return Vlist;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }






        public List<InfoMobiliti> GetAmenitieN(string nombre)
        {
            List<InfoMobiliti> V1list = new List<InfoMobiliti>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("select_amenitie_nombre  @nombre", connection);
                        command.Parameters.AddWithValue("@nombre", nombre);

                   
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                InfoMobiliti i = new InfoMobiliti();
                                i.clv_amenitie = Int32.Parse(rd[0].ToString());
                                i.nombre = rd[1].ToString();
                                i.descripcion = rd[2].ToString();
                                i.activo = bool.Parse(rd[3].ToString());
                                V1list.Add(i);

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Erro select_vista_nombre " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return V1list;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        #endregion

        #region update
        public InfoMobiliti UpdateVendedor(string nombre, int? clv_vendedor, string domicilio, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("update_vendedor @clv_vendedor,@nombre,@domicilio,@activo", connection);

                        command.Parameters.AddWithValue("@clv_vendedor", clv_vendedor);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@domicilio", domicilio);
                        command.Parameters.AddWithValue("@activo", activo);

                        InfoMobiliti i = new InfoMobiliti();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error UpdateVendedor " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return i;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public InfoMobiliti UpdateVista(string nombre, int? clv_vista, string descripcion, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {

                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("update_vista   @clv_vista , @nombre , @descripcion ,@activo", connection);

                        command.Parameters.AddWithValue("@clv_vista", clv_vista);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@descripcion", descripcion);
                        command.Parameters.AddWithValue("@activo", activo);

                        InfoMobiliti i = new InfoMobiliti();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error UpdateVista " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return i;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public InfoMobiliti UpdateAmenitie(string nombre, int? clv_amenitie, string descripcion, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("update_amenitie  @clv_amenitie , @nombre , @descripcion ,@activo", connection);

                        command.Parameters.AddWithValue("@clv_amenitie", clv_amenitie);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@descripcion", descripcion);
                        command.Parameters.AddWithValue("@activo", activo);


                        InfoMobiliti i = new InfoMobiliti();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error UpdateAmenitie " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return i;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public InfoMobiliti UpdateDepartamento(string nombre, int? clv_espacio_departamento, float metros_cuadrados, string contrasenia, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("update_espacio_departamento  @clv_espacio_departamento , @nombre , @contrasenia , @correo ,@activo", connection);

                        command.Parameters.AddWithValue("@clv_espacio_departamento", clv_espacio_departamento);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@metros_cuadrados", metros_cuadrados);
                        command.Parameters.AddWithValue("@activo", activo);




                        InfoMobiliti i = new InfoMobiliti();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error UpdateVendedor " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return i;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public InfoMobiliti UpdateUsuarios(string nombre, int? clv_usuario, string correo, string contrasenia, bool? activo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("update_usuarios  @clv_usuario , @nombre , @contrasenia , @correo ,@activo", connection);

                        command.Parameters.AddWithValue("@clv_usuario", clv_usuario);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@correo", correo);
                        command.Parameters.AddWithValue("@contrasenia ", contrasenia);
                        command.Parameters.AddWithValue("@activo", activo);




                        InfoMobiliti i = new InfoMobiliti();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error UpdateVendedor " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }



                        return i;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        #endregion
    }
}


